#!/usr/bin/env python

from resource import resource
from resource import request
from resource import HTTPError
import conf
import json

class resourceRegistryList(resource):

	registry = {}

	def __init__(self,registry,content = None):
		self.registry = registry
		if content != None:
			resource.__init__(self,content)

	def GET(self,*vpath,**params):

		ct = request.headers.get('Content-Type')

		if ct not in conf.content_type:
			raise HTTPError(400, 'Content-Type nao suportado')

		return json.dumps(self.registry)
