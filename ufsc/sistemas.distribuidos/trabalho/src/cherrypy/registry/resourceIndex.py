#!/usr/bin/env python

from resource import resource

class resourceIndex(resource):

	root = None

	def __init__(self,root,content = None):
		self.root = root
		if content != None:
			resource.__init__(self,content)		


	def GET(self):
		s = 'contador: ' + str(self.getCounter()) + '\n'
		s += '__dict__:\n'
		for i in self.root.__dict__:
			s += '\t' + str(i) + '\n'
		return s
