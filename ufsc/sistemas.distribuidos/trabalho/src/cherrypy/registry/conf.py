#!/usr/bin/env python

import cherrypy

conf = {
	'global' : {
		'server.socket_host': '0.0.0.0',
		'server.socket_port': 8080,
	},
	'/' : {
		'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
		'tools.response_headers.on': True,
		'tools.response_headers.headers': [('Content-Type','application/json')],
	}
}

content_type = (
	'application/json',
	'application/json; charset=UTF-8'
)
