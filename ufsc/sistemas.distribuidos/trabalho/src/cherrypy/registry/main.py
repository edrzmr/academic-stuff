#!/usr/bin/env python
import cherrypy
from conf import conf
from resource import root
from resourceIndex import resourceIndex
from resourceRegistry import resourceRegistry
from resourceRegistryList import resourceRegistryList

registry = {}

root.index = resourceIndex(root,'index')
root.registry = resourceRegistry(registry,'registry')
root.registryList = resourceRegistryList(registry,'registry list')


if __name__ == '__main__':

	cherrypy.quickstart(root,'/',conf)
