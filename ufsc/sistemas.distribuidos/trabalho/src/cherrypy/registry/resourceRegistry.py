#!/usr/bin/env python

from resource import resource
from resource import request
from resource import HTTPError
import conf
import json

class resourceRegistry(resource):

	registry = {}

	def __init__(self,registry,content = None):
		self.registry = registry
		if content != None:
			resource.__init__(self,content)

	def PUT(self):
		ct = request.headers.get('Content-Type')

		if ct not in conf.content_type:
			raise HTTPError(400, 'Content-Type nao implementado')

		self.registry[len(self.registry)+1] = json.loads(request.body.read())
		return json.dumps(len(self.registry))

	def DELETE(self,*vpath,**params):
		ct = request.headers.get('Content-Type')

		if ct not in conf.content_type:
			raise HTTPError(400, 'Content-Type nao implementado')

		resource_id = int(vpath[0])

		if resource_id not in self.registry:
			raise HTTPError(400, 'Recurso nao encontrado')

		del self.registry[resource_id]

		return json.dumps(':)')
