#!/usr/bin/env python

from threading import Thread
from time import sleep
from sys import stderr

import httplib2
import conf
from random import randint

class Emissor(Thread):

	relogio = None
	http = None
	url = 'http://localhost'
	end = False

	def __init__(self,relogio):
		self.http = httplib2.Http()
		self.relogio = relogio
		Thread.__init__(self)

	def geturl(self,dest):
		return self.__geturl(dest)

	def __geturl(self,dest):

		self.relogio.ticket()
		ts = self.relogio.get_timestamp()
		cont = str(ts[conf.estacao-1])
		port = conf.getPortStr(dest)
		
		url = self.url + ':' + conf.getPortStr(dest) + '/message?'
		url += 'm=' + cont + ',' + str(conf.estacao) + '&'
		url += 'v=' + str(ts[0]) + ',' + str(ts[1]) + ',' + str(ts[2])
		return url

	def __put(self,dest):

		method = 'PUT'
		headers = {'Content-Length': '0', 'Content-Type': 'application/json'}
		url = self.__geturl(dest)
		response, content = self.http.request(url,method=method,headers=headers)

	def run(self):
		count = 0
		sleep(10)
		while self.end == False:
			sleep(1 * randint(1,5))
			self.__put(randint(1,3))

	def stop(self):
		self.end = True

