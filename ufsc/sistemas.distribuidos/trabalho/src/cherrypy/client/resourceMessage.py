#!/usr/bin/env python

from resource import resource
from resource import request
from resource import HTTPError
import conf
import json

class resourceMessage(resource):

	root = None

	def __init__(self,root,content = None):
		self.root = root
		if content != None:
			resource.__init__(self,content)


	def __is_deliveryble(self,m):

		print 'ts: ' + str(self.root.relogio.get_timestamp())

		count = 1
		for i in self.root.relogio.get_timestamp():

			if count == m['estacao_origem']:

				print 'analizando estacao de origem'
				if i != (m['relogio_vetor'][count-1] - 1):
					print "if i != (m['relogio_vetor'][count-1] - 1):"
					print "i: " + str(i)
					print "m['relogio_vetor'][count]: " + str(m['relogio_vetor'][count-1])
					print "m: " + str(m)
					print 'count: ' + str(count)

					return False

			else:
				if i < m['relogio_vetor'][count-1]:
					print "if i < m['relogio_vetor'][count-1]:"
					print "i: " + str(i)
					print "m['relogio_vetor'][count]: " + str(m['relogio_vetor'][count-1])
					print "m: " + str(m)
					print 'count: ' + str(count)
					return False

			count += 1

		return True

	def __delivery(self):
		
		print
		count = 0
		for m in self.root.buf_recepcao:
			if self.__is_deliveryble(m) == True:
				self.root.buf_delivery.append(m)
				del self.root.buf_recepcao[count]
				self.root.relogio.update(m['relogio_vetor'])
				self.__delivery()

			else:
				print '* nao eh deliveryble'
				print

			count += 1

	def PUT(self,*vpath,**params):

		print 'handle message put'

		ct = request.headers.get('Content-Type')
		if ct not in conf.content_type:
			raise HTTPError(400, 'Content-Type nao implementado')

		m = {}
		m['ordem_local']    = int(params['m'].split(',')[0])
		m['estacao_origem'] = int(params['m'].split(',')[1])
		l = []
		for i in params['v'].split(','):
			l.append(int(i))
		m['relogio_vetor']  = tuple(l)
		self.root.buf_recepcao.append(m)

		#self.root.relogio.update(m['relogio_vetor'])
		self.__delivery()

		return json.dumps('')

	def GET(self):
		print ''
		print '*** recepcao ***'
		for i in self.root.buf_recepcao:
			print i

		print ''
		print '*** delivery ***'
		for i in self.root.buf_delivery:
			print i

