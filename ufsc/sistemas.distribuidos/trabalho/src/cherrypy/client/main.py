#!/usr/bin/env python
import cherrypy
from resource import root
from resourceIndex import resourceIndex
from resourceMessage import resourceMessage

import emissor

import conf
import relogio

from cherrypy.process import plugins
class uhetPlugin(plugins.SimplePlugin):

	def start(self):
		self.bus.log('uhet start')
		pass

	def stop(self):
		self.bus.log('uhet stop')
		emissor.stop()

uhet = uhetPlugin(cherrypy.engine)
uhet.subscribe()

def start_receptor(emissor,relogio):

	root.relogio = relogio
	
	root.buf_recepcao = []
	root.buf_delivery = []
	
	root.index = resourceIndex(root,'index')
	root.message = resourceMessage(root,'message')

	cherrypy.quickstart(root,'/',conf.conf)

if __name__ == '__main__':

	relogio = relogio.Relogio(
		conf.estacao,
		conf.numero_de_estacoes
	)

	emissor = emissor.Emissor(relogio) 
	emissor.start()

	print emissor.geturl(1)
	print emissor.geturl(2)
	print emissor.geturl(3)

	start_receptor(emissor,relogio)	
