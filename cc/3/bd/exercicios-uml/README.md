Lista de exercícios de modelagem – Padrão UML
=============================================

1. Modele um cadastro de livros e seus autores. Onde cada livro pode ser
composto por 1 ou mais autores e um autor pode escrever vários livros. As
informações sobre cada livro são: código ABNT, título e sinopse.

2. Modele os dados de um cadastro de equipamentos, onde se deseja cadastrar
apenas: código do equipamento, nome do equipamento e tipo de equipamento. O tipo
refere-se a que o equipamento pode ser: computador, ventilador, etc. No final o
modelo deve propiciar ao sistema a possibilidade de emitir uma consulta de
equipamentos por tipo de equipamento.

3. Modele o cadastro de músicas para CD. É necessário armazenar dados sobre:
título do CD, distribuidora, nome das músicas e intérprete de cada música. Uma
música pode estar presente em outros títulos de CD, bem como um intérprete pode
cantar diversas músicas.

4. Crie um cadastro para armazenar clientes, constando: nome e endereço de cada
cliente, onde o endereço é composto de: rua, bairro, cidade e estado.

5. Crie um cadastro de pessoas que registre o histórico de endereços de pessoa.
O cadastro de pessoas é composto por nome e data de nascimento, e o endereço é
composto pelos atributos rua, bairro, cidade e estado. É necessário saber a
partir de que data ele esteve ou está em tal endereço.

6. Criar um cadastro de equipamentos, contendo nome e modelo do equipamento e
uma lista de que materiais compõe este equipamento. Lembrar que um material em
especial pode estar presente em equipamentos diferentes, assim como um
equipamento possui N materiais.

7. Criar um cadastro de equipamentos, contendo nome e modelo do equipamento e
identificação individual de cada equipamento em especial, onde é necessário
registrar número de série e data de compra para o equipamento. Exemplo:

Equipamento                     | N. série         | Comprado
--------------------------------|------------------|------------
Ventilador 110 V, modelo KV 14  | 390293-232-3232  | 01/09/2000
                                | 239392-2321-1232 | 14/09/2001
