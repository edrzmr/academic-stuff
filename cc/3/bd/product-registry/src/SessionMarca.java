import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;


public class SessionMarca {

	private String host = null;
	private String porta = null;
	private String usuario = null;
	private String senha = null;
	private String dbname = null;
	private String dbprefix = null;

	private String driver = null;
	private String url = null;
	private Connection conexao = null;
	private Statement bd = null;
	private ResultSet table = null;


	private void loadConfig() {

		host = "localhost";
		porta = "3306";
		usuario = "uhet";
		senha = "uhet";    
		driver = "org.gjt.mm.mysql.Driver";
		dbname = "gnoia";
		dbprefix = "jdbc:mysql://";
		url = dbprefix + host + ":" + porta + "/" + dbname;
		//"jdbc:mysql://localhost:3306/gnoia"
	}

	private void execute(String sql) {
		try {
			bd.execute(sql);
		} catch (SQLException e) {
			System.out.println("falha na execucao do sql");
			System.out.println(sql);
			e.printStackTrace();
		}
	}

	private Connection conect() {
		try {
			if (conexao == null) {
				Class.forName(driver);
				conexao = (Connection) DriverManager.getConnection(url, usuario, senha);
				bd = (Statement) conexao.createStatement();
				table = null;
			} else {
				if (conexao.isClosed()) {
					conexao = null;
					bd = null;
					table = null;
					return conect();
				}
			}
		} catch (ClassNotFoundException excesao) {
			System.out.println("falha na conex�o :-(");
			excesao.printStackTrace();
		} catch (SQLException excesao) {
			System.out.println("falha na conex�o :-(");
			excesao.printStackTrace();
		}
		return conexao;
	}

	private void disconect() {
		if (conexao != null) {
			try {
				conexao.close();
				conexao = null;
				bd = null;
				table = null;
			} catch (SQLException excesao) {
				System.out.println("falha ao fechar a conex�o :-(");
				excesao.printStackTrace();
			}
		}
	}

	private boolean consultar(BeanMarca b_aux, Statement _bd) {
		
		String sql = "SELECT cod_marca FROM marca WHERE descricao = '" + b_aux.getDescricao() + "';";
		try {
			table = (ResultSet) _bd.executeQuery(sql);
			
				if (table.next()) {
					b_aux.setCodigo(table.getInt("cod_marca"));
					return true;
				}
		} catch (SQLException e) {
			System.out.println("falha na consulta :-(");
			System.out.println(sql);
			e.printStackTrace();
		}
		return false;
	}

	public SessionMarca() {
		super();
		loadConfig();
	}

	public void inserir(BeanMarca novo) {

		String sql = "INSERT INTO marca(descricao) VALUES('" + novo.getDescricao() + "');";
		conect();
		if (!consultar(novo,bd)) {
			execute(sql);
		}
		disconect();
	}

	public void alterar(BeanMarca velha, BeanMarca nova) {

		String sql = "SELECT cod_marca FROM marca WHERE descricao = '" + velha.getDescricao() + "';";

		try {
			conect();
			table = (ResultSet) bd.executeQuery(sql);

			if (table.next()) {
				velha.setCodigo(table.getInt("cod_marca"));
				nova.setCodigo(velha.getCodigo());
			}

			if (!consultar(nova,bd)) {
				sql = "UPDATE marca SET descricao='" + nova.getDescricao() + "' WHERE cod_marca = '" + nova.getCodigo().intValue() + "';";
				bd.execute(sql);	
			}

			disconect();
		} catch (SQLException e) {
			System.out.println("falha na altera��o");
			System.out.println(sql);
			e.printStackTrace();
		}
	};

	public void remover(BeanMarca novo) {
		String sql = "DELETE FROM marca WHERE descricao ='" + novo.getDescricao() + "';"; 
		conect();
		try {
			bd.execute(sql);
		} catch (SQLException e) {
			System.out.println("falha na remo��o :-(");
			System.out.println(sql);
			e.printStackTrace();
		}
		disconect();
	};

	public boolean consultar(BeanMarca b_aux) {

		String sql = "SELECT cod_marca FROM marca WHERE descricao = '" + b_aux.getDescricao() + "';";
		conect();
		try {
			table = (ResultSet) bd.executeQuery(sql);
				if (table.next()) {
					b_aux.setCodigo(table.getInt("cod_marca"));
					disconect();
					return true;
				}
		} catch (SQLException e) {
			System.out.println("falha na consulta :-(");
			System.out.println(sql);
			e.printStackTrace();
		}
		disconect();
		return false;
	}

	public ArrayList letraA() {

		ArrayList array_aux = new ArrayList();
		String sql = "SELECT * FROM marca;";
		conect();
		try {
			table = (ResultSet) bd.executeQuery(sql);
			while (table.next()) {
				BeanMarca marca = new BeanMarca();
				marca.setCodigo(table.getInt("cod_marca"));
				marca.setDescricao(table.getString("descricao"));
				array_aux.add(marca);
			}
		} catch (SQLException e) {
			System.out.println("falha na letra A :-(");
			System.out.println(sql);
			e.printStackTrace();
		}
		disconect();
		return array_aux;
	}

	public ArrayList letraD(String marca) {
		BeanMarca bMarca = new BeanMarca(marca);
		return letraD(bMarca);
	}

	public ArrayList letraD(BeanMarca marca) {
		
		String sql = "SELECT count(marca.cod_marca) FROM marca,produto,marca_produto WHERE produto.cod_produto=marca_produto.cod_produto and marca.cod_marca=marca_produto.cod_marca and marca.descricao='"+ marca.getDescricao() +"';";
		ArrayList list = new ArrayList();

		conect();
		if (consultar(marca,bd)) {
			try {
				table = (ResultSet) bd.executeQuery(sql);

				if (table.next()) {

					String aux = marca.getCodigo() + " | " + marca.getDescricao() + " | " + table.getInt("count(marca.cod_marca)");
					list.add(aux);
				}
			} catch (SQLException e) {
				System.out.println("falha no exercicio D :-(");
				System.out.println(sql);
				e.printStackTrace();
			}
		}
		disconect();
		return list;
	}

	public void inserir(String marca) {
		BeanMarca bMarca = new BeanMarca(marca);
		inserir(bMarca);
	}

	public void alterar(String velha, String nova) {
		BeanMarca bmVelha = new BeanMarca(velha);
		BeanMarca bmNova = new BeanMarca(nova);
		alterar(bmVelha, bmNova);
	}

	public void remover(String marca) {
		BeanMarca bMarca = new BeanMarca(marca);
		remover(bMarca);
	}

	public boolean consultar(String marca) {
		BeanMarca bMarca = new BeanMarca(marca);
		return consultar(bMarca);
	}
}
