public class BeanMarca {

	private Integer codigo;
	private String descricao;

	BeanMarca() {};

	BeanMarca(String desc) {
		setDescricao(desc);
	}

	BeanMarca(String desc, int cod) {
		setDescricao(desc);
		setCodigo(cod);
	}

	public Integer getCodigo() {

		return codigo;
	};

	public String getDescricao() {

		return descricao;
	};

	public void setCodigo(int _codigo) {
		codigo = new Integer(_codigo);
	};

	public void setCodigo(Integer _codigo) {
		codigo = _codigo;
	};

	public void setDescricao(String _descricao) {
		descricao = _descricao;
	};
}
