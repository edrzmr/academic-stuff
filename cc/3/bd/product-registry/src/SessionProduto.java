import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;


public class SessionProduto {

	private String host = null;
	private String porta = null;
	private String usuario = null;
	private String senha = null;
	private String dbname = null;
	private String dbprefix = null;

	private String driver = null;
	private String url = null;
	private Connection conexao = null;
	private Statement bd = null;
	private ResultSet table = null;

	private void loadConfig() {

		host = "localhost";
		porta = "3306";
		usuario = "uhet";
		senha = "uhet";    
		driver = "org.gjt.mm.mysql.Driver";
		dbname = "gnoia";
		dbprefix = "jdbc:mysql://";
		url = dbprefix + host + ":" + porta + "/" + dbname;
		//"jdbc:mysql://localhost:3306/gnoia"
	}

	private Connection conect() {
		try {
			if (conexao == null) {
				Class.forName(driver);
				conexao = (Connection) DriverManager.getConnection(url, usuario, senha);
				bd = (Statement) conexao.createStatement();
			} else {
				if (conexao.isClosed()) {
					conexao = null;
					bd = null;
					table = null;
					return conect();
				}
			}
		} catch (ClassNotFoundException excesao) {
			System.out.println("falha na conex�o :-(");
			excesao.printStackTrace();
		} catch (SQLException excesao) {
			System.out.println("falha na conex�o :-(");
			excesao.printStackTrace();
		}
		return conexao;
	}

	private void disconect() {
		if (conexao != null) {
			try {
				conexao.close();
				bd = null;
				table = null;
			} catch (SQLException excesao) {
				System.out.println("falha ao fechar a conex�o :-(");
				excesao.printStackTrace();
			}
		}
	}

	private boolean consultar(BeanProduto produto, Statement _bd) {

		String sql = "SELECT cod_produto FROM produto WHERE descricao = '" + produto.getDescricao() + "';";
		try {
			table = (ResultSet) _bd.executeQuery(sql);
			if (table.next()) {
				produto.setCodigo(table.getInt("cod_produto"));
				return true;
			}
		} catch (SQLException e) {
			System.out.println("falha na consulta :-(");
			System.out.println(sql);
			e.printStackTrace();
		}
		return false;
	}

	private void getCodigo(BeanProduto produto) {

		String sql = "SELECT cod_produto FROM produto WHERE descricao = '" + produto.getDescricao() + "';";
		try {

			table = (ResultSet) bd.executeQuery(sql);
			if (table.next()){
				produto.setCodigo(table.getInt("cod_produto"));
			}
		} catch (SQLException e) {
			System.out.println("falha na obten��o do codigo do produto do banco");
			System.out.println(sql);
			e.printStackTrace();
		}
	}

	public SessionProduto(){
		super();
		loadConfig();
	}

	public void inserir(BeanProduto produto, BeanMarca marca, String valor) {

		SessionMarca s_marca = new SessionMarca(); 
		String sql = new String();

		conect();
		if (!consultar(produto,bd)) {
			sql = "INSERT INTO produto(descricao) VALUES('" + produto.getDescricao() + "');";
			try {
				bd.execute(sql);
			} catch (SQLException e) {
				System.out.println("falha na inserao :-/");
				System.out.println(sql);
				e.printStackTrace();
			}
		}
		getCodigo(produto);

		//trata a marca, se nao existir insere, salva o codigo no objeto, arrumar tah podre
		if (!s_marca.consultar(marca)) {
			s_marca.inserir(marca);
			s_marca.consultar(marca);
		}

		sql = "INSERT INTO marca_produto(cod_marca,cod_produto,valor_unitario) VALUES('" + marca.getCodigo() + "','" + produto.getCodigo() + "','" + valor +"');";

		try {
			bd.execute(sql);
			//table = (ResultSet) bd.executeQuery(sql);
		} catch (SQLException e) {
			System.out.println("relacionamento jah existente!");
			System.out.println(sql);
			e.printStackTrace();
		}

		disconect();
	}

	public void alterar(BeanProduto velha, BeanProduto nova) {

		String sql = "SELECT cod_produto FROM produto WHERE descricao = '" + velha.getDescricao() + "';";

		try {
			conect();
			bd.execute(sql);
			if (table.next()) {
				velha.setCodigo(table.getInt("cod_produto"));
				nova.setCodigo(velha.getCodigo());
			}
			sql = "UPDATE produto SET descricao='" + nova.getDescricao() + "' WHERE cod_produto = '" + nova.getCodigo().intValue() + "';";
			bd.execute(sql);
			disconect();
		} catch (SQLException e) {
			System.out.println("falha na altera��o");
			System.out.println(sql);
			e.printStackTrace();
		}
	};

	public void remover(BeanProduto novo) {

		consultar(novo);
		String sql_um = "DELETE FROM marca_produto WHERE cod_produto = '" + novo.getCodigo() + "';";
		String sql_dois = "DELETE FROM produto WHERE cod_produto = '" + novo.getCodigo() + "';";
		conect();
		try {
			bd.execute(sql_um);
			bd.execute(sql_dois);
		} catch (SQLException e) {
			System.out.println("falha na remo��o");
			System.out.println(sql_um);
			System.out.println(sql_dois);
			e.printStackTrace();
		}
	};

	public boolean consultar(BeanProduto prod) {

		String sql = "SELECT cod_produto FROM produto WHERE descricao = '" + prod.getDescricao() + "';";
		try {
			conect();
			bd.executeQuery(sql);
				if (table.next()) {
					
					prod.setCodigo(table.getInt("cod_produto"));
					return true;
				}
			disconect();
		} catch (SQLException e) {
			System.out.println("falha na consulta :-(");
			System.out.println(sql);
			e.printStackTrace();
		}
		return false;
	}

	public void inserir(String produto, String marca, String valor) {

		BeanMarca bMarca = new BeanMarca(marca);
		BeanProduto bProduto = new BeanProduto(produto);
		inserir(bProduto,bMarca,valor);
	}

	public void alterar(String velho, String novo) {
		BeanProduto bpVelho = new BeanProduto(velho);
		BeanProduto bpNovo = new BeanProduto(novo);
		alterar(bpVelho,bpNovo);
	}

	public void remover(String produto) {
		BeanProduto bProduto = new BeanProduto(produto);
		remover(bProduto);
	}

	public boolean consultar(String produto) {
		BeanProduto bProduto = new BeanProduto(produto);
		return consultar(bProduto);
	}

	public ArrayList letraB() {

		String sql = "SELECT produto.cod_produto,produto.descricao,marca.descricao FROM marca,produto,marca_produto WHERE produto.cod_produto = marca_produto.cod_produto and marca.cod_marca = marca_produto.cod_marca;";
		ArrayList list = new ArrayList();

		conect();
		try {
			table = (ResultSet) bd.executeQuery(sql);

			while (table.next()) {
				BeanProduto produto = new BeanProduto();
				BeanMarca marca = new BeanMarca();

				produto.setCodigo(table.getInt("cod_produto"));
				produto.setDescricao(table.getString("produto.descricao"));
				marca.setDescricao(table.getString("marca.descricao"));
				String aux = produto.getCodigo().toString() + " | " + produto.getDescricao() + " | " + marca.getDescricao();
				list.add(aux);
			}
		} catch (SQLException e) {
			System.out.println("falha no exercicio B :-(");
			System.out.println(sql);
			e.printStackTrace();
		}
		disconect();
		return list;
	}

	public ArrayList letraC(String marca) {
		BeanMarca bMarca = new BeanMarca(marca);
		return letraC(bMarca);
	}

	public ArrayList letraC(BeanMarca bMarca) {

		String sql = "SELECT produto.descricao FROM marca,produto,marca_produto WHERE produto.cod_produto = marca_produto.cod_produto and marca.cod_marca = marca_produto.cod_marca and marca.descricao = '"+ bMarca.getDescricao() +"';";
		ArrayList list = new ArrayList();

		conect();
		try {
			table = (ResultSet) bd.executeQuery(sql);

			while (table.next()) {
				BeanProduto produto = new BeanProduto();
				BeanMarca marca = new BeanMarca();
				
				produto.setDescricao(table.getString("produto.descricao"));
				String aux = produto.getDescricao();
				list.add(aux);
			}
		} catch (SQLException e) {
			System.out.println("falha no exercicio C :-(");
			System.out.println(sql);
			e.printStackTrace();
		}
		disconect();
		return list;
	}
}
