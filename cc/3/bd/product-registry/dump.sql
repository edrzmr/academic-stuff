-- phpMyAdmin SQL Dump
-- version 2.6.4
-- http://www.phpmyadmin.net
-- 
-- M�quina: localhost
-- Data de Cria��o: 20-Set-2005 �s 03:11
-- Vers�o do servidor: 4.1.14
-- vers�o do PHP: 4.4.0
-- 
-- Base de Dados: `gnoia`
-- 
CREATE DATABASE `gnoia` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE gnoia;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `marca`
-- 

CREATE TABLE `marca` (
  `cod_marca` int(11) NOT NULL auto_increment,
  `descricao` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`cod_marca`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `marca_produto`
-- 

CREATE TABLE `marca_produto` (
  `cod_marca` int(11) NOT NULL default '0',
  `cod_produto` int(11) NOT NULL default '0',
  `valor_unitario` float NOT NULL default '0',
  PRIMARY KEY  (`cod_marca`,`cod_produto`),
  KEY `cod_produto` (`cod_produto`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `produto`
-- 

CREATE TABLE `produto` (
  `cod_produto` int(11) NOT NULL auto_increment,
  `descricao` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`cod_produto`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
