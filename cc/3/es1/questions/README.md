Trabalho de Engenharia de Software
==================================

Autor
-----

- Eder Ruiz Maria

Questionário
------------

1. Faça um pequeno resumo explicando: qual a motivação para o surgimento da
Engenharia de Software, o que é a engenharia de software, quais os passos para
se resolver um problema, qual(is) seu(s) objetivo(s).

 _ES surgiu para contornar a crise do software de 70 e adicionar metodologia de
engenharia a desenvolvimento de software complexo. É uma abordagem do
desenvolvimento de software mais formal, com disciplina e metodologia,
proporcionando um maior planejamento e gerenciamento, para cumprir tais
objetivos ela faz uso de: analise e definição de requisitos, projeto, testes,
entrega do software e manutenção._

2. Quem e quais os papeis dos membros de uma equipe de desenvolvimento?

 _Na equipe de desenvolvimento:_
  - _analista (analise e definição de requisitos, auxilia no projeto);_
  - _projetista (projeto);_
  - _programador (implementação, auxilia no projeto, teste de unidades);_
  - _testador (teste de unidade e teste de integração);_
  - _instrutor (entrega do sistema);_

 _Todos os membros são responsaveis pela manutenção._

3. O que causa a mudança da Engenharia de Software e por que?

 _O tempo ineficiente para conclusão do projeto, ocorre devido a exigencia do
mercado, acaba-se pulando etapas no planejamento, Tecnologias que facilitam o
desenvolvimento (orientação a objetos), Inconsistência no modelo de
desenvolvimento cascata que quando posto em pratica se mostra diferente da
teoria, A nescessidade de refazer e ou alterar a análise de requisitos devido a
erros ocorridos no inicio ou alteração feita pelo cliente._

4. Quais são os elementos base da Engenharia de Software?

 _Processo, metodologia e ferramentas._

5. O que é um processo de desenvolvimento de software e modelo de ciclo de vida?

 - _Processo: sequencia de atividades agrupados geralmente em tarefas e fases,
que são executadas por individuos com responsabilidades bem definidas e possuem
uma entrada e uma saida._

 - _Modelo de ciclo de vida: É uma especificação teórica dos passos e estágios
pelos quais os processos passarão._

6. Quais são as atividades básicas presentes em qualquer processo de
desenvolvimento?

 _Análise e definição dos requisitos,projeto do sistema,projeto do programa,
escrever os programas,testes das unidades, teste de integração, teste do
sistema, entrega do sistema e manutenção._

7. Faça uma analise dos modelos de ciclo de vida descritos no livro da Shari L.
Pfleeger e do Roger S. Pressman(5ed), destacando suas vantagens, desvantagens e
a que problema ele é melhor aplicado.

 - _Cascata: definição clara dos estágios distintos do processo, a passagem dos
estágios só ocorrem quando o estágio anterior estiver concluido, oferece maior
previsibilidade de prazos e custo, melhor planejamento e gerenciamento,
dificuldade em realizar mudanças com o processo em andamento pois os requisitos
sempre mudam. Melhor aplicado quando se tem entendimento claro dos requisitos._

 - *Prototipação: É um modelo interativo (desenvolvedor/usuário e
etapa\_n/etapa\_n-1), permite correção mais agil de erros nas etapas,
principalmente na análise de requisito.*

 - *Espiral: É um meta-modelo, não é estatico, se altera conforme a
nescessidade, o desenvolvido passa varias vezes pelas mesmas fazes, onde
descisões são reavaliadas e alterações no modelo são executadas.*

 - *Evolutivo: A partir dos requisitos iniciais, é elaborado um protótipo que
permite, junto ao cliente explorar novos requisitos, mais comumente utilizado
quando os requisitos não são suficientes para os outros modelos, porém os
sistemas são freqüente mente mal-estruturados e mal-documentados, o processo não
é claro, dificuldade de planejamento e gerenciamento.*

 - *Incremental: desenvolvimento iterativo, em ciclos que permitem revisões de
atividades anteriores, o sistema é particionado em partes independentes que
podem ser entregues á medida que forem ficando prontas e avaliadas.*

8. Faça uma pesquisa para identificar outros modelos de ciclo de vida
existentes, destaque quais são suas vantagens, desvantagens e a que problema
ele é melhor aplicado.

 *Sequencial Linear, Protótipo, RAD, Evolucional, Componentes, Quarta geração*

9. Com base nos estudos feitos sobre esses modelos de ciclo de vida, crie o
seu modelo de ciclo de vida e destaque suas vantagens, desvantagens e a que
problema ele é melhor aplicado.

 *Modelo Bazar: Seria um modelo alternativo talvez muito radical, onde não seria
visando lucro na produção do software, ele nem seria produzido por uma empresa,
é apenas o desejo de desenvolvedores de desenvolver algo usavel, as equipes
seriam por adesão, o desenvolvedor seria voluntario na área que lhe compete, não
seria estabelecido prazos para entrega, o bom andamento dos processos se daria
conforme a dedicação, esforço e comprometimento do voluntário desenvolvedor.*

10. Descreva como é o processo de requisitos.

 *A analise de requisitos é composta pela identificação detalhada das
funcionalidades do sistema (Levantamento de Requisitos) e a respectiva descrição
(Especificação do Sistema) de modo a que os mesmos requisitos possam ser
validados pelos utilizadores finais do sistema.*

11. Quais são os dois tipos de documentos de requisitos e seus respectivos
objetivos?

 - *Funcional: descrições feitas pelo cliente/usuário das funcionalidades do
sistema;*

 - *Não funcional: funcionalidades que estão implicitas nos requisitos
funcionais, tais quais referem-se normalmente a criterios mais técnicos que não
são relatados pelo usuario.*

12. Quais são as categorias dos requisitos e qual o seu impacto no
desenvolvimento do software?

 - *Estudo de viabilidade: verifica a viabilidade do desenvolvimento;*

 - *Levantamento de requisitos: obter os requisitos dos clientes/usuários;*

 - *Analise de requisitos: levantamento sobre a qualidade dos requisitos do
sistema e elaboração de um modelo do sistema (componentes e interfaces);*

 - *Especificação de requisitos: descrição objetiva, precisa e completa dos
requisitos;*

 - *Validação e Verificação dos requisitos: verificação do de se o proposito
esta sendo obedecido e se esta destro das especificações de projeto;*

 - *Gerenciamento de requisitos: controle e localização de mudanças durante e
após o desenvolvimento;*

 - *Documentação: normas e padrões.*

13. Quais são as fontes dos possíveis requisitos?

 *Clientes, usuários, especialistas da área em questão, e também na pesquisa do
problema.*

14. Quais os tipos de requisitos que devem ser considerados quando estamos
elaborando o documento de requisitos?

 *Ambiente físico, interfaces, usuários e fatores humanos, funcionalidade,
documentação, dados, recursos, e segurança.*

15. Quais são as características dos requisitos?

 *Ser consistente, altamente descritivo, sem contradições nem ambiguidades,
facilitar interpretação unica.*

16. Quais são as técnicas para se expressar requisitos e como podemos
identificar a que mais se adeque ao nosso problema?

 - *Descrições estáticas dos requisitos, referência indireta, relações de
recorrência, definição axiomática, expressão como uma linguagem, abstração de
dados;*

 - *Descrição dinâmica, tabelas de decisão, descrição funcional e diagramas de
transição, tabelas de eventos, redes de Petri, abordagem estruturada, abordagem
OO.*
