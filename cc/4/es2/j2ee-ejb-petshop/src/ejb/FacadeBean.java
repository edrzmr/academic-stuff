package ejb;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class FacadeBean implements SessionBean
{
	private SessionContext context;
	private TipoAnimalHome tipoAnimal;
	private AnimalHome animal;

	public void ejbCreate() throws CreateException {
		try
		{
			tipoAnimal = lookupTipoAnimal();
//			animal = lookupTurma();
		}
		catch (NamingException ex)
		{
			throw new CreateException(ex.getMessage());
		}
	}
	
	public void createTipoAnimal(Details info) throws RemoteException
	{
		try
		{
			System.out.println("tipo animal = " + info.getName());
			tipoAnimal.create(info.getId(), info.getName());
			System.out.println("tipo animal criado = " + info.getName());
		}
		catch (Exception e)
		{
			throw new RemoteException(e.getMessage());
		}
	}
	
	public void removeTipoAnimal(Details info) throws RemoteException
	{
		try
		{
			tipoAnimal.findByPrimaryKey(info.getId()).remove();
			System.out.println("animal removido");
		}
		catch (Exception e)
		{
			throw new RemoteException(e.getMessage());
		}
	}
	
	public Collection getTipoAnimal () throws RemoteException
	{
	    Collection tipoAnimais;
	    Vector tipoAnimaisTransfer;
	    TipoAnimal tpA;
	    Details d;
	    Iterator it;
	    
	    tipoAnimaisTransfer = null;
	    try
	    {
	        tipoAnimais =  tipoAnimal.findAll();
			if (tipoAnimais != null)
			{
				tipoAnimaisTransfer = new Vector();
				it = tipoAnimais.iterator();
				while (it.hasNext())
				{
					tpA = (TipoAnimal) it.next();
					d = new Details(tpA.getId(), tpA.getName());
					tipoAnimaisTransfer.add(d);
				}
			}
	    }
	    catch (Exception e)
	    {
	        throw new RemoteException(e.getMessage());
	    }
	    return tipoAnimaisTransfer;
	}
	
//	public void getTipoAnimal(Details info) throws RemoteException;
	
/*	public void addAlunoToTurma(Details turma, Details aluno) throws RemoteException {
		LocalTurma t;
		LocalAluno a;
		
		try {
			t = turmaHome.findByPrimaryKey(turma.getId());
			// Cria aqui o aluno, e insere na turma.
			a = alunoHome.create(aluno.getId(), aluno.getName());
			t.addAluno(a);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
	}*/

/*	public void removeAlunoFromTurma(Details turma, Details aluno) throws RemoteException {
		LocalTurma t;
		LocalAluno a;
		
		try {
			t = turmaHome.findByPrimaryKey(turma.getId());
			a = alunoHome.findByPrimaryKey(aluno.getId());
			t.removeAluno(a);
			// Destroi o aluno, ap�s remover da turma.
			a.remove();
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
	}*/

	/*public Collection getAlunosFromTurma(Details turma) throws RemoteException {
		Collection localAlunos;
		Vector remoteAlunos;
		LocalAluno la;
		Details d;
		Iterator it;
		
		remoteAlunos = null;
		try {
			localAlunos =  alunoHome.findByTurma(turma.getId());
			if (localAlunos != null) {
				remoteAlunos = new Vector();
				it = localAlunos.iterator();
				while (it.hasNext()) {
					la = (LocalAluno)it.next();
					d = new Details(la.getId(), la.getName());
					remoteAlunos.add(d);
				}
			}
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		return remoteAlunos;
	}*/
	
	// M�todos auxiliares
	private TipoAnimalHome lookupTipoAnimal() throws NamingException {
		Context initial = new InitialContext();
		return (TipoAnimalHome)initial.lookup("java:comp/env/ejb/tipoAnimal");
	}

/*	private LocalTurmaHome lookupTurma() throws NamingException {
		Context initial = new InitialContext();
		return (LocalTurmaHome)initial.lookup("java:comp/env/ejb/turma");
	}*/
	
	// M�todos padr�es do SessionBean
	public void setSessionContext(SessionContext arg0) throws EJBException,
			RemoteException {
		this.context = arg0;
	}

	public void ejbRemove() throws EJBException, RemoteException {
	}

	public void ejbActivate() throws EJBException, RemoteException {
	}

	public void ejbPassivate() throws EJBException, RemoteException {
	}
}
