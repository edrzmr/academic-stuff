package ejb;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface FacadeHome extends EJBHome {

	public Facade create() throws RemoteException, CreateException;
}
