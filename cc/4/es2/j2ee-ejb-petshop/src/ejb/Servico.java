package ejb;

import javax.ejb.EJBLocalObject;

public interface Servico extends EJBLocalObject
{
	public String getId();
	public String getName();
	public String getPrice();
}
