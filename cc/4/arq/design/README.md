- ***Universidade Estadual do Oeste do Paraná***
- ***Ciência da Computação - Arquitetura e Organização de Computadores***
- ***Profa Fabiana Frata Furlan Peres - 07/04/2005***

Resenha: Exploração do Design de Espaço usando T & D-Bench
==========================================================

Autores
-------

- André Slompo
- Diogo Henrique Heidemman
- Eder Ruiz Maria
- Fagner B. De Oliveira
- Marcelo De Souza Abondanza

Resenha
=======

Existem diversas ferramentas para design de hardware e arquitetura de hardware, 
temos linguagens descritivas (tanto de hardware quanto de arquitetura),
simuladores de performance e situações. Todas essas ferramentas foram projetadas
visando uso comercial e de pesquisa, mas são utilizadas na educação.

Os cursos que ministram a disciplina arquitetura de computadores tem prazos
apertados e cargas de conteúdo altas. E as ferramentas comentadas acima possuem
curva de aprendizado que variam de duas semanas a dois meses, o que torna
inviável o aprofundamento em qualquer uma dessas ferramentas (fator tempo torna
inviável).

Pensando nestes problemas foi proposto um software que auxilie a pesquisa e os
meios acadêmicos, ou seja tenha curva de aprendizado veloz, seja simples e não
demande muito conhecimento. O ambiente comercial foi descartado pois envolvia
requisitos incompatíveis com os outros dois ambientes.

O software proposto foi o TDSim que utiliza tecnologia T&D Workbench, este
software implementa uma linguagem de script, com pouquissimas palavras
reservadas, entre 6 e 10, com as quais é possível simular ambientes
processadores, incluindo mono-ciclos, multi-ciclos e máquinas Pipelined.

O TDSim possui dois modos de funcionamento o modo completo e o modo componente.

O modo componente foi desenvolvido especialmente para suprir as necessidades dos
estudantes, já que trabalha com componentes, mesmo os componentes de
componentes, como ULAs, multiplexadores, entre outros. Estes componentes podem
ser testados sozinho, ou linkados em grupos formando circuitos.

O modo completo trabalha com processadores completos, e tem alguns já
implementados e prontos para o estudo, como multi-ciclo, mono-ciclo e pipelined.
O usuário pode testar diversos espaços de exploração com este software.

O sistema disponibiliza notações gráficas sobre desempenho e perca de dados no
geral.

Este sistema teve excelente aceitação nas classes de aula nas quais foram
utilizadas e conseguiu sucesso no intuito de unir os requisitos de um programa
de pesquisa com um programa de ensino.
