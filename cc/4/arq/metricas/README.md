Utilização de Métricas Independentes de Hardware
================================================

Métricas independentes de hardware são as tentativas de se utilizar métodos de
avaliar a performance não levando em conta o tempo de execução. Esses métodos
são utilizados quando se faz necessária à comparação de diferentes conjuntos de
instruções e analise de cada um deles para concluir sobre a performance de cada
um dos conjuntos.

Uma forma de aplicar isso é usar o tamanho do código como uma medida de
velocidade, logo, o menor código gerado, a maquina geradora será a mais rápida.
Isto é um equivoco, pois o tamanho do código gerado é importante em relação ao
consumo de memória durante a execução, não podendo ser levado em conta para
avaliação de performance.

O que ocorre atualmente é, em muitas vezes, o contrario disso, onde as maquinas
mais modernas contêm instruções que geram códigos maiores mas que são executados
mais rapidamente.

Um exemplo disso é uma comparação entre os códigos gerados pelos computadores
CDC6600 e o B5500, onde o primeiro gera códigos três vezes maiores que o segundo
e os executa seis vezes mais rápido.

Uma forma legitima de utilizar a comparação entre o tamanho de código seria
fazê-lo para a escolha entre códigos diferentes para a mesma arquitetura, mesmo
assim, não haveria uma relação valida para se extrair um resultado de
performance do código escolhido valido.

Bibliografia
------------

- David A. Patterson e John L. Hennessy; Organização e projeto de computadores,
A interface Hardware/Software.

- <http://cial.csie.ncku.edu.tw/course/2005_Spring_Computer_Organization/CD/
Content/COD3e/InMoreDepth/IMD4-Using-Hardware-Independent-Metrics.pdf>
