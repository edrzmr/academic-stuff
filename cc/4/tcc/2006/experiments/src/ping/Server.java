import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

	private Socket client = null;
	private ServerSocket server = null;
	private DataOutputStream saida = null;
	private DataInputStream entrada = null;

	private void error(String str) {
		System.err.print("[SERVER] error: ");
		System.err.println(str);
	}

	private void msg(String str) {
		System.out.print("[SERVER] msg: ");
		System.out.println(str);
	}

	private void printBA(byte b[]) {
		System.out.print("[SERVER] payload: ");
		for (int i = 0 ; i < (b.length) ; i++){
			System.out.print(b[i] + " ");
		}
		System.out.println();
	}

	public Server(int porta) {
		this.estabelecerConexao(porta);
	}

	public void estabelecerConexao(int porta) {
		try {
			this.server = new ServerSocket(porta);
			msg("aguardando conexao");
			this.client = this.server.accept();
			this.client.setSendBufferSize(1000000000);
			this.client.setReceiveBufferSize(1000000000);
			msg("conexao estabelecida");
			this.saida = new DataOutputStream(this.client.getOutputStream());
			this.entrada = new DataInputStream(this.client.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			error("public void estabelecerConexao(int porta, int tam_buffer)");
		}
	}

	public void fecharConexao() {
		try {
			this.saida.close();
			this.entrada.close();
			this.server.close();
			this.client.close();
			msg("conexao fechada");
		} catch (IOException e) {
			e.printStackTrace();
			error("public void close_connection()");
		}
	}

	public byte[] fazerPacote(int tam) {
		try {
			return (new byte[tam]);
		} catch (OutOfMemoryError e) {
			error("falta de memoria para alocar o buffer");
			this.fecharConexao();
			System.exit(0);
		}
		return null;
	}

	public byte[] receber() {
		try {
			int tam = this.entrada.readInt();
			/* System.out.print("int recebido: " + tam); */
			byte b[] = fazerPacote(tam);
			this.entrada.readFully(b);
			System.out.print(" | tamanho do array: " + b.length);
			return b;
		} catch (EOFException e) {
			this.error("conexao fechada pelo cliente");
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			this.error("public byte[] receber()");
			System.exit(1);
		}
		return null;
	}

	public void enviar(byte b[]) {
		try {
			this.saida.writeInt(b.length);
			this.saida.write(b);
			System.out.println(" | tamanho do array enviado: " + b.length);
		} catch (IOException e){
			e.printStackTrace();
			this.error("public void enviar (byte b[])");
			System.exit(1);
		}
	}

	public void run() {
		msg("iniciando permuta de pacotes");
		while (true){
			byte b[] = this.receber();
			this.enviar(b);
			System.gc();
		}
	}

	public static void main(String[] args) {
		Server sss = new Server(6669);
		sss.setPriority(Thread.MAX_PRIORITY);
		sss.start();
	 }
}
