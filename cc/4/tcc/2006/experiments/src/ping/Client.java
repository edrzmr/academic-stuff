import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client extends Thread {

	private Socket client = null;
	private DataInputStream entrada = null;
	private DataOutputStream saida = null;
	private RandomAccessFile file = null;

	private final int VARIACAO = 1024;
	private final int MAX = 10 * 1024 * 1024;
	private final int N_AMOSTRAS = 100;
	private int TAM = 0;


	private void error(String str) {
		System.err.print("[CLIENT] error: ");
		System.err.println(str);
	}

	private void msg(String str) {
		System.out.print("[CLIENT] msg: ");
		System.out.println(str);
	}

	private void printBA(byte b[]) {
		System.out.print("[CLIENT] payload: ");
		for (int i = 0 ; i < (b.length) ; i++){
			System.out.print(b[i] + " ");
		}
		System.out.println();
	}

	public Client(String ip, int porta, String filename) {
		estabelecerConexao(ip, porta, filename);
	}

	public void gravarLog(int tamanho_do_pacote, long tempo_decorrido) {
		System.out.print(tamanho_do_pacote);
		System.out.print(" || ");
		System.out.println(tempo_decorrido);
		
	}

	public void estabelecerConexao(String ip, int porta, String filename) {
		try {
			this.client = new Socket(InetAddress.getByName(ip),porta);
			this.client.setSendBufferSize(1000000000);
			this.client.setReceiveBufferSize(1000000000);
			msg("estabelecendo conexao");
			this.entrada = new DataInputStream(this.client.getInputStream());
			this.saida = new DataOutputStream(this.client.getOutputStream());
			this.file = new RandomAccessFile(new File(filename),"rw");
		} catch (UnknownHostException e) {
			e.printStackTrace();
			error("provavelmente nao ha ninguem escutando");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			error("public void estabelecerConexao(String ip, int porta, int tam_buffer)");
			System.exit(1);
		}
	}

	public void fecharConexao() {
		try {
			this.entrada.close();
			this.saida.close();
			this.client.close();
			this.file.close();
			msg("conexao fechada");
		} catch (IOException e) {
			e.printStackTrace();
			error("public void fechar Conexao()");
		}
	}

	public byte[] fazerPacote(int tam) {
		try {
			return (new byte[tam]);
		} catch (OutOfMemoryError e) {
			error("falta de memoria para alocar o buffer");
			this.fecharConexao();
			System.exit(0);
		}
		return null;
	}

	public void enviar(byte b[]) {
		try {
			this.saida.writeInt(b.length);
			// System.out.print("int enviado: " + b.length);
			this.saida.write(b);
			// System.out.print(" | tamanho array: " + b.length);
		} catch (IOException e) {
			e.printStackTrace();
			error("public void enviar(byte b[])");
		}
	}

	public byte[] receber() {
		try {
			int tam = this.entrada.readInt();
			System.out.print(" | int recebido: " + tam);
			byte b[] = fazerPacote(tam);
			this.entrada.readFully(b);
			// System.out.print(" | tamanho array: " + b.length);
			return b;
		} catch (IOException e){
			e.printStackTrace();
			error("public byte[] receber()");
		}
		return null;
	}

	public void ping(int tamanho_do_pacote) {

		this.enviar(fazerPacote(tamanho_do_pacote));
		byte b[] = this.receber();
	}

	public void log(String s){
		try {
			file.writeBytes(s + '\n');
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	public long testarAmostra() {

		long tempo_inicial = System.currentTimeMillis();
		for (int i = 0; i < N_AMOSTRAS; i++) {

			this.ping(TAM);
		}

		long tempo_final = System.currentTimeMillis();
		return tempo_final - tempo_inicial;
	}

	public void run() {
		int sequencia = 1;
		long tempo = 0;
		this.msg(":) iniciando experimento");
		while (TAM < MAX) {
			System.out.print("[ping] tamanho: " + TAM + " bytes sequencia: " + sequencia + " tempo: ");
			tempo = this.testarAmostra();
			System.out.println(tempo + " ms");
			this.log(TAM + ":" + tempo);
			this.TAM += this.VARIACAO;
			sequencia++;
		}
		this.msg(":) experimento concluido");
	}

	public static void main(String[] args) {
		Client ccc = new Client(args[0], 6669, args[1]);
		ccc.setPriority(Thread.MAX_PRIORITY);
		ccc.start();
	}
}
