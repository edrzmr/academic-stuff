/* vim: set ts=4 sw=4: */

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>

#define NOME "calc"
#define NUM_OPT 2
#define NUM_ARGUMENTOS NUM_OPT*2+1
#define PI 3.141592654

int
help(void)
{
	printf("Sintaxe: %s <opcoes>\n", NOME);
	printf("Opcoes:\n");
	printf("\t-f <numero de processos filho>\n");
	printf("\t-l <numero de vezes que o loop deve executar>\n");
	return 0;
}

int
calc_sin(unsigned loop)
{
	unsigned i;
	float ret;
	for (i = 0; i < loop; i++) {

		ret = sin(PI / 2) / cos(0) * sin(PI / 2);
	}

	(void) ret;
	return 0;
}

int
parir(const unsigned filhos, const unsigned loop)
{
	unsigned i;
	int pid;
	for (i = 0; i < filhos; i++) {

		pid = fork();
		if (pid == 0) { /* filho */

			calc_sin(loop);
			exit(0);

		} else if (pid < 0) { /* erro */

			printf("erro, problema com o fork(<0)\n");
			exit(0);
/*
		} else if (pid > 0){
			printf("pai\n");
*/
		}
	}
	return 0;
}

int
cronometrar(const unsigned filhos, const unsigned loop)
{
	unsigned i;
	struct timeval tempo_inicial, tempo_final, tempo_diff;

	gettimeofday(&tempo_inicial, NULL);
	parir(filhos, loop);

	for (i = 0; i < filhos; i++) {
		int status;
		wait(&status);
		if (!WIFEXITED(status))
			exit(1);
	}
	gettimeofday(&tempo_final, NULL);
	timersub(&tempo_final, &tempo_inicial, &tempo_diff);
	
	/* <n> <atual> <tempo> */
	printf("%lu.%03lu\n", tempo_diff.tv_sec, tempo_diff.tv_usec / 1000);
	return 0;
}

int
main(int argc, char *argv[])
{
	int opt;
	unsigned filhos;
	unsigned loop;

	if (argc == NUM_ARGUMENTOS) {
		while ((opt = getopt(argc, argv, "f:l:")) != -1) {

//          printf("opt: %i \n", opt);
			switch (opt) {
			case 'f':
				filhos = atoi(optarg);
				break;
			case 'l':
				loop = atoi(optarg);
				break;
			default:
				printf("erro\n");
				printf("f: %i\n", filhos);
				printf("l: %i\n", loop);
				exit(0);
				break;
			}
		}
/*
//      printf("filhos: %i\n", filhos);
//      printf("loop: %i\n", loop);
*/
		return cronometrar(filhos, loop);
	} else {
		help();
		exit(0);
	}
/*
	printf("numero de filhos: %i\n", numero_de_filhos);
	printf("numero de pais: %i\n", numero_de_pais);
*/
	return 0;
}
