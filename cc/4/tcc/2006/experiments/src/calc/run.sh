#!/bin/bash

INI=$1
VARIANCIA=$2
MAX=$3
N_LOOP=$4
N_AMOSTRAS=$5
LOG_FILE=$6

POINTER=$INI
AMOSTRA=$INI
UHETCALC="./calc"

if [ $# = 6 ];
then
	echo "#<$1> <$2> <$3> <$4> <$5> <$6>" | tee $6

	#loop para amostras
	while [ $POINTER -lt $MAX ];
do
		I=0
		ACM=0
		while [ $I -lt $N_AMOSTRAS ]
		do
			AUX=$($UHETCALC -f $POINTER -l $N_LOOP)
			ACM=$(echo $ACM+$AUX | bc)
			let I=$I+1
		done

		echo -n "$AMOSTRA " | tee -a $LOG_FILE; let AMOSTRA=$AMOSTRA+1
		echo -n "$POINTER " | tee -a $LOG_FILE; let POINTER=$POINTER+$VARIANCIA
		echo $ACM           | tee -a $LOG_FILE
	done

else
	echo "uso: $0 <ini> <var> <max> <n_loop> <n_amostras> <log_file>"
	echo "ini: numero de filhos iniciais"
	echo "var: variancia do numero de filhos"
	echo "max: numero de filhos com que termina o experimento"
	echo "n_loop: numero de vezes que deve ocorrer o calculo matematica em cada filho"
	echo "n_amostras: numero de amostras para uma mesma quantidade de filhos"
	echo "log_file: arquivo onde sera armazenado o log"
fi
