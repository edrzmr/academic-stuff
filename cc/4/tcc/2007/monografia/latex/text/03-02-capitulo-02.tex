\chapter{Comunicação Serial}
\label{cap:cs}

\section{Comunicação de Dados}

A distância que um dado sinal percorre em um computador varia de alguns
milímetros, como no caso de conexões de um simples CI (\textit{Circuito
Integrado}), até vários centímetros quando a conexão de sinais envolve, por
exemplo, uma placa mãe com conectores para diversos circuitos. Para estas
distâncias o dado digital pode ser transmitido diretamente. Exceto em
computadores muito rápidos, os projetistas não se preocupam com o formato e
espessura dos condutores, ou com as características analógicas dos sinais de
transmissão \cite{livro:serial}.

No entanto, frequentemente os dados devem ser enviados para fora dos circuitos
que constituem o computador. Nesses casos, as distâncias envolvidas transcendem
os poucos centímetros internos dos computadores e alcançam centenas de metros e
quilômetros. Infelizmente, com o aumento das distâncias entre a fonte e o
destino aumenta também a dificuldade de estabelecer uma transmissão de dados
precisa. Isso é resultado de distorções elétricas dos sinais que trafegam
através de condutores longos, e de ruídos adicionados ao sinal que se propagam
através do meio de transmissão. Embora alguns cuidados devam ser tomados na
troca de dados dentro de um computador, o grande problema ocorre quando dados
são transferidos para dispositivos fora dos circuitos do computador. Nesse caso
a distorção e o ruído podem ser tão severos que a informação é perdida
\cite{livro:serial}.

A Comunicação de Dados estuda os meios de transmissão de mensagens digitais para
dispositivos externos ao circuito originador da mensagem. Dispositivos Externos
são geralmente circuitos com fonte de alimentação independente dos circuitos
relativos a um computador ou outra fonte de mensagens digitais. Como regra, a
taxa de transmissão máxima permissível de uma mensagem é diretamente
proporcional a potência do sinal, e inversamente proporcional ao ruído. A função
de qualquer sistema de comunicação é fornecer a maior taxa de transmissão, com a
menor potência e com o menor ruído possíveis \cite{livro:serial}.

\section{Canais de Comunicação}

Um canal de comunicação é um caminho sobre o qual a informação pode trafegar.
Ela pode ser definida por uma linha física (fio) que conecta dispositivos de
comunicação, ou por um rádio, laser, ou outra fonte de energia radiante.

Em comunicação digital, a informação é representada por \textit{bits} de dados
individuais, que podem ser encapsulados em mensagens de vários \textit{bits}. Um
\textit{byte} (conjunto de 8 \textit{bits}) é um exemplo de uma unidade de
mensagem que pode trafegar através de um canal digital de comunicações. Uma
coleção de \textit{bytes} pode ser agrupada em um \textit{frame} ou outra
unidade de mensagem de maior nível. Esses múltiplos níveis de encapsulamento
facilitam o reconhecimento de mensagens e interconexões de dados complexos
\cite{livro:serial}.

Um canal no qual a direção de transmissão é inalterada é referida como canal
\textit{simplex}. Por exemplo, uma estação de rádio é um canal \textit{simplex}
porque ela sempre transmite o sinal para os ouvintes e nunca é permitido a
transmissão inversa \cite{livro:serial}.

Um canal \textit{half-duplex} é um canal físico simples no qual a direção pode
ser revertida. As mensagens podem fluir nas duas direções, mas nunca ao mesmo
tempo. Em uma chamada telefônica, uma parte fala enquanto a outra escuta. Depois
de uma pausa, a outra parte fala e a primeira escuta. Falar simultaneamente
resulta em sons que não podem ser compreendidos \cite{livro:serial}.

Um canal \textit{full-duplex} permite que mensagens sejam trocadas
simultaneamente em ambas as direções. Pode ser interpretado como dois canais
\textit{simplex}, um canal direto e um canal reverso, conectados aos mesmos
pontos \cite{livro:serial}.

\section{Comunicação Serial}

A maioria das mensagens digitais são mais longas que alguns poucos
\textit{bits}. Por não ser prático nem econômico transferir todos os
\textit{bits} de uma mensagem simultaneamente, a mensagem é quebrada em partes
menores e transmitida seqüencialmente. A transmissão \textit{bit-serial}
converte a mensagem em um \textit{bit} por vez através de um canal. Cada
\textit{bit} representa uma parte da mensagem. Os \textit{bits} individuais são
então rearranjados no destino para compor a mensagem original. Em geral, um
canal irá passar apenas um \textit{bit} por vez. A transmissão
\textit{bit-serial} é normalmente chamada de transmissão serial, e é o método de
comunicação escolhido por diversos periféricos de computadores
\cite{livro:serial}.

A transmissão \textit{byte-serial} converte 8 \textit{bits} por vez através de 8
canais paralelos. Embora a taxa de transferência seja 8 vezes mais rápida que na
transmissão \textit{bit-serial}, são necessários 8 canais, e o custo poderá ser
maior do que 8 vezes para transmitir a mensagem. Quando as distâncias são
curtas, é factível e econômico usar canais paralelos como justificativa para as
altas taxas de transmissão. A interface Centronics de impressoras é um caso
típico de transmissão \textit{byte-serial} \cite{man:serial2}.

\section{Taxa de Transferência (\textit{Baud Rate})}

A taxa de transferência refere-se a velocidade com que os dados são enviados
através de um canal e é medido em transições elétricas por segundo. Na
comunicação serial, ocorre uma transição de sinal por \textit{bit}, e a taxa de
transferência e a taxa de \textit{bit} (\textit{bit rate}) são idênticas. Nesse
caso, uma taxa de 9600 \textit{bauds} corresponde a uma transferência de 9600
dados por segundo, ou um período de aproximadamente, 104 us (1/9600 s)
\cite{apostila:serial}.

Outro conceito é a eficiência do canal de comunicação que é definido como o
número de \textit{bits} de informação utilizável (dados) enviados através do
canal por segundo. Ele não inclui \textit{bits} de sincronismo, formatação, e
detecção de erro que podem ser adicionados a informação antes da mensagem ser
transmitida, e sempre será no máximo igual a um. Na Figura \ref{fig:serial1}
pode ser observada a taxa de transferência serial \cite{apostila:serial}.

\figura{Taxa de transferência \cite{apostila:serial}}{serial1}{images/serial01}
{0.4}

\section{Transmissão Assíncrona x Transmissão Síncrona}

Geralmente, dados serializados não são enviados de maneira uniforme através de
um canal. Ao invés disso, pacotes com informação regulares são enviados seguidos
de uma pausa. Os pacotes de dados binários são enviados dessa maneira,
possivelmente com comprimentos de pausa variável entre pacotes, até que a
mensagem tenha sido totalmente transmitida \cite{apostila:serial}.

O circuito receptor dos dados deve estar ciente do momento apropriado para ler
os \textit{bits} individuais desse canal, saber exatamente quando um pacote
começa e quanto tempo decorre entre \textit{bits}. Quando essa temporização for
conhecida, o receptor é dito estar sincronizado com o transmissor, e a
transferência de dados precisa tornar-se possível. Falhas na manutenção do
sincronismo durante a transmissão irão causar a corrupção ou perda de dados
\cite{apostila:serial}.

Duas técnicas básicas são empregadas para garantir a sincronização correta. Em
sistemas síncronos, canais separados são usados para transmitir dados e
informação de tempo. O canal de temporização transmite pulsos de \textit{clock}
para o receptor. Através da recepção de um pulso de \textit{clock}, o receptor
lê o canal de dado e armazena o valor do \textit{bit} encontrado naquele
momento. O canal de dados não é lido novamente até que o próximo pulso de
\textit{clock} chegue. Como o transmissor é responsável pelos pulsos de dados e
de temporização, o receptor irá ler o canal de dados apenas quando comandado
pelo transmissor, e portanto a sincronização é garantida. A Figura
\ref{fig:serial2} ilustra melhor esta técnica \cite{apostila:serial}.

\figura{Canal de dados e \textit{clock} \cite{apostila:serial}}{serial2}
{images/serial02}{0.55}

Existem técnicas que compõem o sinal de \textit{clock} e de dados em um único
canal. Isso é usual quando transmissões síncronas são enviadas através de um
modem. Dois métodos no qual os sinais de dados contém informação de tempo são:
codificação NRZ (\textit{Non-Return-to-Zero}) e a codificação Manchester
\cite{apostila:serial}.

Em sistemas assíncronos, a informação trafega por um canal único. O transmissor
e o receptor devem ser configurados antecipadamente para que a comunicação se
estabeleça a contento. Um oscilador preciso no receptor irá gerar um sinal de
\textit{clock} interno que é igual (ou muito próximo) ao do transmissor. Para o
protocolo serial mais comum, os dados são enviados em pequenos pacotes de 10 ou
11 \textit{bits}, dos quais 8 constituem a mensagem \cite{apostila:serial}.

Quando o canal está em repouso, o sinal correspondente no canal tem um nível
lógico `1'. Um pacote de dados sempre começa com o nível lógico `0'
(\textit{start bit}) para sinalizar ao receptor que uma transmissão foi
iniciada. O \textit{start bit} inicializa um temporizador interno no receptor
avisando que a transmissão começou e que serão necessários pulsos de
\textit{clock}. Seguido do start \textit{bit}, 8 \textit{bits} de dados de
mensagem são enviados na taxa de transmissão especificada. O pacote é concluído
com os \textit{bits} de paridade e de parada (\textit{stop bit}). Na Figura
\ref{fig:serial3} pode ser observado um pacote serial típico
\cite{apostila:serial}.

\figura{Pacote serial \cite{apostila:serial}}{serial3}{images/serial03}{0.35}

O comprimento do pacote de dados é pequeno em sistemas assíncronos para
minimizar o risco do oscilador do transmissor e do receptor variar. Quando
osciladores a cristal são utilizados, a sincronização pode ser garantida sobre
os 11 \textit{bits} de período. A cada novo pacote enviado, o \textit{start bit}
reinicia a sincronização, portanto a pausa entre pacotes pode ser longa
\cite{apostila:serial}.

\section{Paridade e \textit{Checksum}}

Ruídos e distúrbios elétricos momentâneos podem causar mudanças nos dados quando
estão trafegando pelos canais de comunicação. Se o receptor falhar ao detectar
isso, a mensagem recebida será incorreta, resultando em conseqüências
possivelmente sérias. Como uma primeira linha de defesa contra erros de dados,
eles devem ser detectados. Se um erro pode ser sinalizado, pode ser possível
pedir que o pacote com erro seja reenviado, ou no mínimo prevenir que os dados
sejam interpretados como corretos. Se uma redundância na informação for enviada,
1 ou 2 \textit{bits} de erros podem ser corrigidos pelo hardware no receptor
antes que o dado chegue ao seu destino \cite{apostila:serial}.

O \textit{bit} de paridade é adicionado ao pacote de dados com o propósito de
detecção de erro. Na convenção de paridade-par (\textit{even-parity}), o valor
do \textit{bit} de paridade é escolhido de tal forma que o número total de
dígitos `1' dos dados adicionado ao \textit{bit} de paridade do pacote seja
sempre um número par \cite{apostila:serial}.

Na recepção do pacote, a paridade do dado precisa ser recalculada pelo hardware
local e comparada com o \textit{bit} de paridade recebido com os dados. Se
qualquer \textit{bit} mudar de estado, a paridade não irá coincidir, e um erro
será detectado. Se um número par de \textit{\textit{bit}s} for trocado, a
paridade coincidirá e o dado com erro será validado. Contudo, uma análise
estatística dos erros de comunicação de dados tem mostrado que um erro com
\textit{bit} simples é muito mais provável que erros em múltiplos \textit{bits}
na presença de ruído randômico. Portanto, a paridade é um método confiável de
detecção de erro. Um exemplo de cálculo de paridade pode ser observado na Figura
\ref{fig:serial4}.

\figura{Exemplo de paridade \cite{apostila:serial}}{serial4}{images/serial04}
{0.3}

Outro método de detecção de erro envolve o cálculo de um \textit{checksum}
quando mensagens com mais de um \textit{byte} são transmitidas pelo canal de
comunicação. Nesse caso, os pacotes que constituem uma mensagem são adicionados
aritmeticamente. Um número de checksum é adicionado a seqüência do pacote de
dados de tal forma que a soma dos dados mais o \textit{checksum} é zero
\cite{apostila:serial}.

Quando recebido, os dados devem ser adicionados pelo processador local. Se a
soma do pacote der resultado diferente de zero, ocorreu um erro. Na ocorrência
de erros é improvável (mas não impossível) que qualquer corrupção de dados
resultem em \textit{checksum} igual a zero. Um exemplo de cálculo de paridade
pode ser observado na Figura \ref{fig:serial5}

\figura{Exemplo de \textit{checksum} \cite{apostila:serial}}{serial5}
{images/serial05}{0.3}

Podem ocorrer erros que não sejam apenas detectados, mas também sejam corrigidos
se um código extra for adicionado a seqüência de dados do pacote. A correção de
erros em uma transmissão, abaixa a eficiência do canal, e o resultado é uma
queda na transmissão \cite{apostila:serial}.

\section{RS232}

RS é uma abreviação de \textit{Recommended Standard}, que relata uma
padronização de uma interface comum para comunicação de dados entre
equipamentos. Criada no início dos anos 60, por um comitê conhecido atualmente
como \textit{Electronic Industries Association} (EIA). Neste período, a
comunicação de dados compreendia a troca de dados digitais entre um computador
central (\textit{mainframe}) e terminais de computador remoto, ou entre dois
terminais sem o envolvimento do computador. Estes dispositivos poderiam ser
conectados por meio de linha telefônica, e consequentemente necessitavam de um
modem em cada lado para realizar a codificação e decodificação dos sinais
\cite{livro:serial}.

Destas idéias surgiu o padrão RS232, que especifica as tensões, temporizações e
funções dos sinais, um protocolo para troca de informações, e as conexões
mecânicas. A mais de 30 anos, desde que essa padronização foi desenvolvida, a
EIA publicou três modificações. A mais recente, EIA232E, foi publicada em 1991.
Ao lado da mudança de nome de RS232 para EIA232, algumas linhas de sinais foram
renomeadas e várias linhas novas foram definidas. Embora tenha sofrido poucas
alterações, muitos fabricantes adotaram diversas soluções mais simplificadas que
tornaram impossível a utilização da padronização proposta. As maiores
dificuldades encontradas pelos usuários na utilização da interface RS232 incluem
pelo menos um dos seguintes fatores \cite{livro:serial}:

\begin{itemize}

\item A ausência ou conexão errada de sinais de controle, resultam em problemas
e travamentos da comunicação.

\item Função incorreta de comunicação para o cabo em uso, resultam em inversão
das linhas de Transmissão e Recepção, bem como a inversão de uma ou mais linhas
de controle (\textit{handshaking}).

\end{itemize}

\subsection{Definição de Sinal}

Se a norma EIA232 completa for implementada, o equipamento que faz o
processamento dos sinais é chamado DTE (\textit{Data Terminal Equipment}),
usualmente um computador ou terminal), possui um conector DB25 macho, e utiliza
22 dos 25 pinos disponíveis para sinais ou terra. O equipamento que faz a
conexão (normalmente uma interface com a linha telefônica) é denominado de DCE
(\textit{Data Circuit-terminating Equipment}), usualmente um modem, possui um
conector DB25 fêmea, e utiliza os mesmos 22 pinos disponíveis para sinais e
terra. Um cabo de conexão entre dispositivos DTE e DCE contém ligações em
paralelo, não necessitando mudanças na conexão de pinos. Se todos os
dispositivos seguissem essa norma, todos os cabos seriam idênticos, e não
haveria possibilidade de haver conexões incorretas. Pode ser observado na Figura
\ref{fig:rs232-1} as conexões descritas \cite{apostila:serial}.

\figura{Modelo de conexão RS232 \cite{apostila:serial}}{rs232-1}{images/rs232-1}
{0.4}

Na Figura \ref{fig:rs232-2} é apresentada a definição dos sinais para um
dispositivo DTE (usualmente um micro PC). Os sinais mais comuns são apresentados
em negrito.

\figura{Sinais para um Dispositivo DTE \cite{apostila:serial}}{rs232-2}
{images/rs232-2}{0.5}

\subsection{Características dos Sinais}

Sinais com tensão entre $-3$ volts e $-25$ volts com relação ao terra são
considerados nível lógico ``1'' (condição marca), e tensões entre +3 volts e +25
volts são considerados nível lógico ``0'' (condição espaço). A faixa de tensões
entre $-3$ volts e +3 volts é considerada uma região de transição para o qual o
estado do sinal é indefinido. A Figura \ref{fig:rs232-3} ilustra as
características dos sinais \cite{apostila:serial}.

\figura{Características dos Sinais \cite{apostila:serial}}{rs232-3}
{images/rs232-3}{0.6}

Se forem inseridos LEDs (\textit{Light Emitting Diode}) ou circuitos de teste
para visualizar o estado dos sinais, o sinal de tensão cairá em magnitude e
poderá afetar o rendimento da interface se o cabo for longo. Também é relevante
notar que alguns periféricos são alimentados com os próprios sinais da interface
para não utilizar fonte de alimentação própria, isto diminui a imunidade a
ruídos \cite{apostila:serial}.

\subsection{Temporização dos Sinais}

A norma EIA232 especifica uma taxa máxima de transferência de dados de 20.000
\textit{bits} por segundo (o limite usual é 19200 bps). \textit{Baud rates}
fixos não são fornecidos pela norma, contudo, os valores comumente usados são
300, 1200, 2400, 4800, 9600 e 19200 bps \cite{apostila:serial}.

Mudanças no estado dos sinais de nível lógico ``1'' para ``0'' ou vice-versa
devem seguir características abaixo:

\begin{itemize}

\item Sinais que entram na zona de transição durante uma mudança de estado devem
atravessar essa região com direção ao estado oposto sem reverte-la ou reentrar;

\item Para os sinais de controle, o tempo na zona de transição deve ser menor do
que 1 ms;

\end{itemize}

Para sinais de temporização, o tempo para atravessar a zona de transição deve
ser:

\begin{itemize}

\item Menor do que 1 ms para períodos de \textit{bits} maiores que 25 ms;

\item 4\% do período de um \textit{bit} para períodos entre 25 ms e 125 us;

\item Menor do que 5 us para períodos menores que 125 us.

\end{itemize}

\subsection{Conversores de Sinal TTL - RS232}

A maioria dos equipamentos digitais utilizam níveis TTL
(\textit{Transistor-Transistor Logic}) ou CMOS (\textit{Complementary
Metal-Oxide-Semiconductor}). Portanto, o primeiro passo para conectar um
equipamento digital a uma interface RS232 é transformar níveis TTL (0 a 5 volts)
em RS232 e RS232 em TTL. Isto é feito por conversores de nível
\cite{livro:serial}.

Existem diversos tipos de equipamentos digitais que utilizam o \textit{driver}
1488 (de TTL para RS232) e o \textit{receiver} 1489 (de RS232 para TTL). Estes
CIs contém 4 inversores de um mesmo tipo, sejam \textit{drivers} ou
\textit{receivers}. O \textit{driver} necessita duas fontes de alimentação +7,5
volts a +15 volts e 7,5 volts a 15 volts. Isto é um problema onde somente uma
fonte de +5 volts é utilizada \cite{livro:serial}.

Um outro CI que está sendo largamente utilizado é o MAX232, que inclui um
circuito de \textit{charge pump} capaz de gerar tensões de +10 volts e 10 volts
a partir de uma fonte de alimentação simples de +5 volts, bastando para isso
alguns capacitores externos. Este CI também possui dois \textit{receivers} e
dois \textit{drivers} no mesmo encapsulamento \cite{livro:serial}.

Nos casos onde serão implementados somente as linhas de transmissão e de
recepção de dados, não seria necessário 2 \textit{chips} e fontes de alimentação
extras \cite{livro:serial}.

\subsection{Cabo \textit{Null Modem}}

Um cabo \textit{null modem} é utilizado para conectar dois DTEs. Isto é
comumente usado como um meio econômico para transferir arquivos entre
computadores utilizando protocolos Zmodem e Xmodem. Ele também pode ser
utilizado em diversos sistemas de desenvolvimento \cite{apostila:serial}.

Um método de conexão de cabo \textit{null modem} é apresentado na Figura
\ref{fig:rs232-4}. Apenas 3 fios são necessários (TxD, RxD e GND). A teoria de
operação é razoavelmente simples. O princípio é fazer o DTE pensar que está
conectado a um modem. Qualquer dado transmitido do DTE deve ser recebido no
outro extremo e vice-versa. O sinal de terra (SG) também deve ser conectado ao
terra comum dos dois DTEs.

\figura{Conexão \textit{Null Modem} \cite{apostila:serial}}{rs232-4}
{images/rs232-4}{0.5}

O sinal DTR (\textit{Data Terminal Ready}) é conectado com os sinais DSR
(\textit{Data Set Ready}) e CD (\textit{Carrier Detect}) nos dois extremos.
Quando o sinal DTR for ativado (indicando que o canal de comunicação está
aberto), imediatamente os sinais DSR e CD são ativados. Neste momento o DTE
detecta que o Modem Virtual ao qual está conectado está pronto e que foi
detectado uma portadora no outro modem. O DTE precisa se preocupar agora com os
sinais RTS (\textit{Request To Send}) e CTS (\textit{Clear To Send}). Como os 2
DTEs se comunicam na mesma velocidade, o fluxo de controle não é necessário e
consequentemente essas 2 linhas são conectadas juntas em cada DTE. Quando o
computador necessita transmitir um dado, ele ativa a linha RTS como estão
conectadas juntas, imediatamente recebe a resposta que o outro DTE está pronto
pela linha CTS \cite{apostila:serial}.

O sinal RI (\textit{Ring Indicator}) não está conectado em nenhum extremo. Esta
linha é utilizada apenas para informar ao DTE que existe um sinal de chamada
telefônica presente. Como não existe modem conectado a linha telefônica este
sinal pode permanecer desconectado \cite{apostila:serial}.

\subsection{Controle de Fluxo de Dados}

Se a conexão entre um DTE e um DCE for diversas vezes mais rápida do que a
velocidade entre os DCEs, em algum momento os dados transmitidos do DTE serão
perdidos. Nesse caso o controle de fluxo de dados é utilizado, e o controle de
fluxo pode ser feito por \textit{hardware} ou por \textit{software}
\cite{livro:serial}.

O controle do fluxo de dados por \textit{software}, também conhecido como
XON/XOFF utiliza 2 caracteres ASCII; XON representado pelo caracter ASCII 17 e
XOFF representado pelo caracter ASCII 19. O modem tem normalmente um buffer
pequeno e quando completado envia o caracter XOFF para avisar o DTE, que deve
parar de enviar dados. Uma vez que o modem estiver pronto para receber mais
dados ele envia o caracter XON e o DTE enviará mais dados. Este tipo de controle
de fluxo tem como vantagem não necessitar linhas adicionais, às linhas TxD
(\textit{Transmitted Data}) e RxD (\textit{Received Data}). A desvantagem é que
o protocolo de comunicação que não poderá utilizar os caracteres ASCII $17$ e
$19$ em suas mensagens  \cite{livro:serial}.

O controle do fluxo de dados por \textit{hardware}, também conhecido como
RTS/CTS utiliza 2 linhas extras em seu cabo serial além das 2 linhas para
transmissão de dados. DTE ativa a linha RTS quanto necessita enviar dados. Se o
modem tem espaço para receber esse dado, irá responder ativando a linha CTS e o
DTE começará a enviar dados. Se o modem não tem espaço para receber dados a
linha CTS não é ativada  \cite{livro:serial}.

\section{RS485}

Da mesma forma como ocorreu com RS232, nome oficial deste padrão é EIA485. O
EIA485 é um padrão de transmissão de dados de uma classe conhecida como ``forma
diferencial'', ideal para a transmissão de dados em altas taxas de transmissão,
em longas distâncias, até mesmo em condições de interferência eletromagnética,
dentro dos limites estipulados pela norma \cite{revista:e}.

A transmissão diferencial anula os efeitos de variação de terra e ruídos em uma
linha de transmissão, que comumente ocorrem nestes aparelhos. Um amplificador
operacional em modo diferencial verifica a diferença das tensões nos seus
terminais de entrada. O cabeamente é trançado, desta forma quando ocorrer algum
tipo de indução eletromagnética no cabo, esta induz igualmente nos dois
condutores elevando a tensão igualmente na entrada do amplificador diferencial,
porém, como a diferença das tensões na sua entrada é verificada, não haverá
alteração na tensão resultante \cite{revista:e}.

Para ilustrar pode-se considerar o seguinte exemplo: condição de entrada $A = +5
V$ e entrada $B = -5V$, a tensão resultante é $A - B = +5 - (-5) = 10V$; podemos
supor que um ruído induziu $+2V$ nos condutores, então $A = +7V$ e entrada $B =
-3V$, a tensão resultante é $A - B = +7 - (-3) = 10 V$. Isto torna este padrão
adequado para rede \textit{fieldbus} \cite{revista:e}.

O Padrão EIA485 é um dos padrões mais versáteis de interface seriais do EIA. É
uma extensão do EIA422.

Este padrão permite a montagem de uma rede de comunicação sobre dois fios,
habilitando uma comunicação serial de dados confiável \cite{rs485}:

\begin{itemize}

\item distância de até 1200 metros (4000 pés);

\item velocidade de até 10 Mbps;

\item até 32 nós na mesma linha de comunicação;

\item expansível até 256 nós, conforme restrições.

\end{itemize}

Se comparado com o EIA232, este padrão tem um menor custo devido a possibilidade
de uso de fontes de alimentação assimétricas, enquanto o RS232 exige o uso de
fontes simétricas nos transmissores e receptores \cite{rs485}. É importante
ressaltar que os computadores convencionais normalmente não possuem interface
EIA485, porém é totalmente possível a conversão entre os padrões EIA232 e
EIA485, por meio da conversão das caracteristicas elétricas.

No entanto a taxa máxima de transmissão de dados e o máximo comprimento não
podem serem alcançados ao mesmo tempo. Para um cabo de par trançado especial a
taxa máxima é 90 Kbps em 1200 metros. O comprimento máximo de cabo a 10 Mbps é
menor que 6 metros \cite{rs485}.

Uma rede EIA485 típica é apresentada na Figura \ref{fig:rs485-1}.

\figura{Rede EIA485 \cite{rs485}}{rs485-1}{images/rs485-1}{0.35}

O uso típico do padrão EIA485 é um único computador (Mestre) conectado a
diversos dispositivos endereçáveis (Escravos) que compartilham o mesmo canal
físico, este endereçamento é tratado pela unidade remota \cite{rs485}.

Embora o uso de comunicação Mestre-Escravo sejá algo comum para o EIA485, o
padrão especifica apenas as características elétricas do \textit{driver} e do
\textit{receiver}. Não esta incluso no padrão quaisquer protocolo de comunicação
ou qualquer outro fim \cite{rs485}.
