#ifndef _PARPORT_H
#define _PARPORT_H
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/io.h>

#define PORTA 0x378
#define TRUE 1
#define FALSE 0
#define LIGADO 1
#define DESLIGADO 0

struct porta
{
	int open;
	unsigned char value;
};

int parport_open(struct porta *pp);
int parport_close(struct porta *pp);
struct porta *parport_create();
int dispositivo_ligar(struct porta *p, int d);
int dispositivo_desligar(struct porta *p, int d);

#endif
