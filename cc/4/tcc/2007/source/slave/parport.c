/* vim: set ts=4 sw=4: */

#include "parport.h"

int
parport_open(struct porta *pp)
{
	int result = ioperm(PORTA, 3, 1);
	/*
	 * 0x378 - Endereço Inicial de Permissão de acesso
	 * 3 - Endereço Final de Permissão de acesso (0x378, 9, A)
	 * 1 - Ativa Permissão para o programa
	 * Retorna 1 se permite acesso, 0 senão
	 */

	if (result != 0) {
		printf("ooops, problemas em abrir a porta paralela :-/\n");
		exit(1);
	} else {
		printf("porta aberta, pode mandar bala :D\n");
		pp->open = TRUE;
	}
}

int
parport_close(struct porta *pp)
{
	/*
	 * 0x378 - Endereço Inicial de Permissão de acesso
	 * 3 - Endereço Final de Permissão de acesso (0x378, 9, A)
	 * 0 - Ativa Permissão para o programa
	 * Retorna 1 se permite acesso, 0 senão
	 */
	int result = FALSE;

	if (pp->open == TRUE) {
		result = ioperm(PORTA, 3, 0);
		printf("porta fechada com sucesso :D\n");
		free(pp);
		pp = NULL;
		return 0;
	}
	printf("tentativa de fechar a porta nao aberta, :-/\n");
	return FALSE;
}

struct porta *
parport_create()
{
	int i;
	struct porta *pp = (struct porta *) malloc(sizeof(struct porta));

	pp->open = FALSE;
	pp->value = 0x00;

	return pp;
}

int
dispositivo_ligar(struct porta *p, int d)
{
	if ((d >= 0) && (d <= 7)) {

		p->value = p->value | (1 << d);
		printf("value: %i | d: %i\n", p->value, d);
		outb(p->value, PORTA);

		return p->value;
	}
}

int
dispositivo_desligar(struct porta *p, int d)
{
	p->value = p->value & ~(1 << d);
	printf("value: %i | d: %i\n", p->value, d);
	outb(p->value, PORTA);
	return p->value;
}

int
dispositivo_teste_on(struct porta *p)
{
	outb(0xFF, PORTA);
}

int
dispositivo_teste_off(struct porta *p)
{
	outb(0x00, PORTA);
}
