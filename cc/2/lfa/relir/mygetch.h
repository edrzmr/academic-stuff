/* vim: set ts=4 sw=4: */

#ifndef __MYGETCH_H__
#define __MYGETCH_H__

#include <unistd.h>
#include <stdio.h>
#include <termios.h>

int mygetch(void);

#endif /* __MYGETCH_H__ */
