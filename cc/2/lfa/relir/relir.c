/* vim: set sw=4 ts=4: */

#include "relir.h"

/* aloca um box_char */
box_char *
aloca_box_char(char c)
{
	box_char *atual = (box_char *) malloc(sizeof(box_char));
	atual->c = c;
	atual->prox = NULL;
	return atual;
}

/* aloca um box_term */
box_term *
aloca_box_term(char t, char d_nt)
{
	box_term *atual = (box_term *) malloc(sizeof(box_term));
	atual->t = t;
	atual->d_nt = d_nt;
	atual->t_prox = NULL;
	atual->nt_prox = NULL;
	return atual;
}

/* aloca um box_producao */
box_producao *
aloca_box_producao(char e_nt, char t, char d_nt)
{
	box_producao *atual = (box_producao *) malloc(sizeof(box_producao));
	atual->e_nt = e_nt;
	atual->t = t;
	atual->d_nt = d_nt;
	atual->prox = NULL;
	return atual;
}

void
add_box_producao(box_producao **H, box_producao *aux)
{
	if ((*H) == NULL)
		(*H) = aux;
	else {
		aux->prox = (*H);
		(*H) = aux;
	}
}

void
add_box_char(box_char **H, box_char *aux)
{
	if ((*H) == NULL)
		(*H) = aux;
	else {
		aux->prox = (*H);
		(*H) = aux;
	}
}

void
add_box_term(box_term **H, box_term *aux)
{
	if ((*H) == NULL)
		(*H) = aux;
	else {
		aux->t_prox = (*H);
		(*H) = aux;
	}
}

box_producao *
get_simbolo(box_producao **H, char S)
{
	box_producao *atual = (*H);

	while ((atual != NULL) && (atual->e_nt != S))
		atual = atual->prox;

	return atual;
}

int
is_char_in_box_char(box_char **T, char c)
{
	box_char *atual = (*T);

	while ((atual != NULL) && (atual->c != c))
		atual = atual->prox;

	if (atual == NULL)
		return FALSE;
	else
		return TRUE;
}

int
is_palavra_in_term(box_char **T, char *palavra)
{
	int aux, i = 0;

	while (palavra[i] != '\0') {

		aux = is_char_in_box_char(T, palavra[i]);
		i++;
		if (aux == FALSE)
			/* se retornar FALSE eh pq deu pau. */
			return FALSE;
	}

	/* retornou TRUE pq deu certo. */
	return TRUE;
}

void
destroi_box_char(box_char **H)
{
	box_char *atual = (*H);
	box_char *ant;

	while (atual != NULL) {

		ant = atual;
		atual = atual->prox;
		free(ant);
	}
	H = NULL;
}

void
destroi_box_producao(box_producao **H)
{
	box_producao *atual = (*H);
	box_producao *ant;

	while (atual != NULL) {

		ant = atual;
		atual = atual->prox;
		free(ant);

	}
	(*H) = NULL;
}

void
destroi_box_term(box_term **H)
{
	box_term *atual = (*H);
	box_term *ant;

	while (atual != NULL) {

		ant = atual;
		atual = atual->t_prox;
		destroi_box_char(&(ant->nt_prox));
		free(atual);
	}
	(*H) = NULL;
}

void
print_box_char(box_char **H)
{
	box_char *aux = (*H);
	while (aux != NULL) {

		printf("%c, ", aux->c);
		aux = aux->prox;
	}
	printf("\n");
}

void
print_box_producao(box_producao **H)
{

	box_producao *aux = (*H);

	while (aux != NULL) {

		printf("%c => %c%c\n", aux->e_nt, aux->t, aux->d_nt);
		aux = aux->prox;
	}
}

void
print_producao(relir *R)
{
	box_term *aux = R->AUX.HT;

	while (aux != NULL) {

		printf("%c %c\n", aux->t, aux->d_nt);
		aux = aux->t_prox;
	}
}

void
limpa_producao(relir *R)
{
	R->AUX.e_nt = '\0';
	destroi_box_term(&(R->AUX.HT));
}

void
expandir_producao(relir *R, char e_nt)
{
	box_producao *aux_producao = R->P;
	box_term *aux_term = R->AUX.HT;
	box_term *box;

	limpa_producao(R);

	while (aux_producao != NULL) {
		if (aux_producao->e_nt == e_nt) {

			box = aloca_box_term(aux_producao->t, aux_producao->d_nt);
			add_box_term(&aux_term, box);
		}
		aux_producao = aux_producao->prox;
	}
}

char
is_char_in_producao(relir *R, char t)
{
	box_term *aux = R->AUX.HT;

	while (aux != NULL) {

		if (aux->t == t)
			return aux->d_nt;
		else
			return '0';
	}
	return '0';
}
