/* vim: set ts=4 sw=4: */

#include <string.h>

#include "relir.h"
#include "mygetch.h"
#include "getstring.h"

void
ler_nao_terminais(relir *R)
{
	uchar c;
	printf("Entre com os simbolos nao terminais (ESC para terminar): \n");
	while ((c = mygetch()) != ESC) {
		printf("%c\n", c);
		add_box_char(&(R->NT), aloca_box_char((char)c));
	}
}

void
ler_terminais(relir *R)
{
	uchar c;
	printf("Entre com os simbolos terminais (ESC para terminar): \n");
	while ((c = mygetch()) != ESC) {
		printf("%c\n", c);
		add_box_char(&(R->T), aloca_box_char((char)c));
	}
}

void
ler_producao(relir *R)
{
	uchar e_nt, t, d_nt;
	printf("Entre com as producoes\n");
	while ((e_nt = mygetch()) != ESC) {
		printf("%c\n", e_nt);
		t = mygetch();
		if (t == ESC)
			break;
		printf("%c\n", t);
		d_nt = mygetch();
		if (d_nt == ESC)
			break;
		printf("%c\n", d_nt);
		printf("> proxima producao (ESC para finalizar))\n");
		add_box_producao(&(R->P),
						 aloca_box_producao((char)e_nt, (char)t, (char)d_nt));
	}
	print_box_producao(&(R->P));
}

void
ler_palavra(char *palavra)
{
	int i;
	for (i = 0; i < MAX_STRING; i++)
		palavra[i] = '\0';
	printf("Entre com a palavra a ser reconhecida: \n");
	strcpy(palavra, getstring());
}

int
get_gramatica(relir *R)
{
	ler_nao_terminais(R);
	ler_terminais(R);
	ler_producao(R);
	return 0;
}

int
valida_palavra(relir *R, char INI, char *palavra)
{
	int i = 0;
	if (is_palavra_in_term(&(R->T), palavra)) {
		while (palavra[i] != '\0') {
			expandir_producao(R, INI);
			INI = is_char_in_producao(R, palavra[i]);
			if (INI == '0')
				return FALSE;
			else
				i++;
		}
		return TRUE;
	} else
		return FALSE;
}

void
relir_inicializar(relir *R)
{
	R->P = NULL;
	R->NT = NULL;
	R->T = NULL;
	R->AUX.HT = NULL;
	R->AUX.e_nt = '\0';
}

void
relir_finalizar(relir *R)
{
	destroi_box_char(&(R->NT));
	destroi_box_char(&(R->T));
	destroi_box_producao(&(R->P));
	destroi_box_term(&(R->AUX.HT));
}

int
main(void)
{
	relir R;
	char palavra[MAX_STR];
	uchar flag = 0;
	int aux;
	relir_inicializar(&R);
	get_gramatica(&R);
	while (flag != ESC) {
		ler_palavra(palavra);
		aux = valida_palavra(&R, 'S', palavra);
		printf("%i\n", aux);
		printf("ESC para sair, qualquer tecla para continuar\n");
		flag = mygetch();
	}
	relir_finalizar(&R);
	return 0;
}
