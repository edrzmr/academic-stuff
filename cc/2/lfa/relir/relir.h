#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

#define MAX_STRING 100
#define MAX_BUFFER 100
#define MAX_MP 100

#define ESC 48
#define ENTER 10

#ifndef _RELIR_H
#define _RELIR_H

typedef unsigned char uchar;

typedef char string[MAX_STRING];

typedef struct box_char
{
	char c;
	struct box_char *prox;

} box_char;

/*
 * S->aB
 * nao terminal a esquerda: S
 * terminal: a
 * nao terminal a direita: B
 */
typedef struct box_producao
{
	char e_nt;
	char t;
	char d_nt;
	struct box_producao *prox;
} box_producao;

typedef struct box_term
{
	char t;
	char d_nt;
	struct box_term *t_prox;
	box_char *nt_prox;
} box_term;

typedef struct temp
{
	char e_nt;
	box_term *HT;
} temp;

typedef struct relir
{
	box_char *NT;
	box_char *T;
	box_producao *P;
	temp AUX;
} relir;

void destroi_box_char(box_char **H);
void destroi_box_producao(box_producao **H);
void destroi_box_term(box_term **H);

box_producao *aloca_box_producao(char e_nt, char t, char d_nt);
box_char *aloca_box_char(char c);
box_term *aloca_box_term(char t, char d_nt);

void add_box_producao(box_producao **H, box_producao *aux);
void add_box_char(box_char **H, box_char *aux);
void add_box_term(box_term **H, box_term *aux);

box_producao *get_simbolo(box_producao **H, char S);

int is_palavra_in_term(box_char **T, char *palavra);
int is_char_in_box_char(box_char **T, char c);

void print_box_char(box_char **T);
void print_box_producao(box_producao **H);
void print_producao(relir *R);

/* int valida_palavra(relir *R, char INI, char *palavra); */
void limpa_producao(relir *R);
void expandir_producao(relir *R, char e_nt);
char is_char_in_producao(relir *R, char t);

#endif
