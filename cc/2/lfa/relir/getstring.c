/* vim: set ts=4 sw=4: */

#include "getstring.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char *
getstring(void)
{
	char s[MAX_STR];
	char *uhet;
	int tam = 0;
	int aux = 0;

	while ((tam < MAX_STR) && (aux != ENTER)) {

		aux = getchar();
		if (aux != ENTER) {
			s[tam] = (char) aux;
			tam++;
		}
		s[tam] = '\0';
	}

	uhet = (char *) malloc(sizeof(char[MAX_STR]));
	strcpy(uhet, s);
	return uhet;
}
